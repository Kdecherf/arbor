# Copyright 2007-2008 Bo Ørsted Andresen
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require python [ blacklist=none has_lib=true multibuild=false with_opt=true ]

SUMMARY="XML C Parser and Toolkit v2"
HOMEPAGE="http://www.xmlsoft.org"
BASE_URI="http://www.w3.org/XML"
SCHEMA_TEST_URI="${BASE_URI}/2004/xml-schema-test-suite/xmlschema"
TEST_VERSION="20130923"
TAR1="2002-01-16"
TAR2="2004-01-14"

DOWNLOADS="ftp://www.xmlsoft.org/${PN}/${PNV}.tar.gz
    ${BASE_URI}/Test/xmlts${TEST_VERSION}.tar.gz
    ${SCHEMA_TEST_URI}${TAR1}/xsts-${TAR1}.tar.gz
    ${SCHEMA_TEST_URI}${TAR2}/xsts-${TAR2}.tar.gz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/news.html"
UPSTREAM_CHANGELOG="${HOMEPAGE}/ChangeLog.html"

LICENCES="MIT"
SLOT="2.0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    examples
"

DEPENDENCIES="
    build+run:
        app-arch/xz
        dev-libs/icu:=
        sys-libs/readline:=
        sys-libs/zlib[>=1.2.5-r1]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( README.tests TODO_SCHEMAS )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Revert-Change-calls-to-xmlCharEncInput-to-set-flush-.patch
)

src_unpack() {
    unpack ${PNV}.tar.gz

    # test suite
    edo cd "${WORK}"
    unpack xmlts${TEST_VERSION}.tar.gz
}

src_prepare() {
    # prevent fetching with wget during src_test
    edo ln -s "${FETCHEDDIR}"/xsts-{${TAR1},${TAR2}}.tar.gz xstc/

    # there is no flag to choose a python abi, instead configure will use the
    # abi obtained from /usr/bin/python. Hence, we'll force configure to use
    # /usr/bin/python$(python_get_abi) instead.
    edo sed -i -e "s:\(bin/python\):\1$(python_get_abi):" configure.ac

    option python && \
        edo sed -i -e "s:^PYTHON_INCLUDES=:&$(python_get_incdir):" configure.ac

    autotools_src_prepare
}

src_configure() {
    # icu required for chromium
    econf \
        --disable-static \
        --enable-ipv6 \
        --with-history \
        --with-icu \
        --with-readline \
        --with-threads \
        --with-zlib=/usr/$(exhost --target) \
        $(option_with python python /usr/$(exhost --target)/bin/python$(python_get_abi))
}

src_install() {
    default
    option python && python_bytecompile

    # The package build system installs stuff to /usr/share/doc/${PNV}; move
    # it to .../${PNVR} as necessary
    if [[ ${PNV} != ${PNVR} ]]; then
        edo mv "${IMAGE}"/usr/share/doc/${PNV}/* "${IMAGE}"/usr/share/doc/${PNVR}
        edo rmdir "${IMAGE}"/usr/share/doc/${PNV}
    fi
    if option python; then
        edo mv "${IMAGE}"/usr/share/doc/${PN}-python-${PV} "${IMAGE}"/usr/share/doc/${PNVR}/python
    fi

    # devhelp doesn't support out-of-source builds
    edo cp -pPR "${WORK}"/doc/devhelp "${IMAGE}"/usr/share/doc/${PNVR}/html/

    if ! option doc; then
        edo rm -r "${IMAGE}"/usr/share/gtk-doc
        edo rm -r "${IMAGE}"/usr/share/doc/${PNVR}/html
    fi

    if ! option examples; then
        edo rm -r "${IMAGE}"/usr/share/doc/${PNVR}/examples
        if option python; then
            edo rm -r "${IMAGE}"/usr/share/doc/${PNVR}/python/examples
        fi
    fi
}

