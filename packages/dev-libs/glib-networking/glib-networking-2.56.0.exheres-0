# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require meson [ meson_minimum_version=0.43 ]

SUMMARY="A simple network library"
HOMEPAGE="https://www.gnome.org/"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    gnome-proxy [[
        description = [ support for GNOME proxy configuration ]
        requires = [ libproxy ]
    ]]
    libproxy
    pkcs11 [[
        description = [ Support for the Cryptographic Token Interface PKCS #11 ]
    ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.4]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.55.1]
        dev-libs/gnutls[>=3.3.5]
        gnome-proxy? ( gnome-desktop/gsettings-desktop-schemas )
        libproxy? ( net-libs/libproxy:1[>=0.3.1] )
        pkcs11? ( dev-libs/p11-kit:1[>=0.20] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/4be630b1769a0827b093435ff3f45287d4e8b77a.patch
    "${FILES}"/681c8dc515a6e8ff3b4440a982bc947686b53065.patch
    "${FILES}"/remove-tests-that-require-networking.patch
)

MESON_SRC_CONFIGURE_PARMAS=(
    '-Dca_certificates_path=/etc/ssl/certs/ca-certificates.crt'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gnome-proxy gnome_proxy_support'
    'libproxy libproxy_support'
    'pkcs11 pkcs11_support'
)

