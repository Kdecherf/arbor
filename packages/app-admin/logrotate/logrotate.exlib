# Copyright 2008-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.xz ] systemd-service

export_exlib_phases src_install

SUMMARY="Rotates, compresses and mails system logs"

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    cron [[ description = [ Install launcher script to cron.daily path ] ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/popt[>=1.5]
        sys-apps/acl
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-werror
    --with-acl
    --with-compress-command=/usr/$(exhost --target)/bin/gzip
    --with-compress-extension=.gz
    --with-default-mail-command=/usr/$(exhost --target)/bin/mail
    --with-state-file-path=/var/lib/logrotate.status
    --with-uncompress-command=/usr/$(exhost --target)/bin/gunzip
    --without-selinux
)

logrotate_src_install() {
    insinto /usr
    dobin logrotate
    doman logrotate.8
    dodoc examples/*

    if option cron; then
        exeinto /etc/cron.daily
        doexe "${FILES}"/logrotate.cron
    fi
    install_systemd_files

    insinto /etc
    doins "${FILES}"/logrotate.conf

    # NOTE(somasis) logrotate errors trying to rotate /var/log/wtmp on musl
    #               because musl does not support utmp/wtmp.
    if [[ $(exhost --target) != *-musl* ]];then
        cat >> "${IMAGE}"/etc/logrotate.conf <<EOF
# no packages own lastlog or wtmp -- we'll rotate them here
/var/log/wtmp {
    monthly
    create 0664 root utmp
    rotate 1
}
EOF
    fi

    echo '# system-specific logs may be also be configured here.' >> "${IMAGE}"/etc/logrotate.conf


    keepdir /etc/logrotate.d
}

